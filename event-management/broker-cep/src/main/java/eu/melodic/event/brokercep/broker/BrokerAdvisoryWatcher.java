/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0, unless
 * Esper library is used, in which case it is subject to the terms of General Public License v2.0.
 * If a copy of the MPL was not distributed with this file, you can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.event.brokercep.broker;

import eu.melodic.event.brokercep.BrokerCepService;
import eu.melodic.event.util.PasswordUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.DataStructure;
import org.apache.activemq.command.DestinationInfo;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.jms.*;

@ConditionalOnProperty(name="brokercep.enable-advisory-watcher", matchIfMissing = true)
@Service
@Slf4j
public class BrokerAdvisoryWatcher implements MessageListener, InitializingBean {
	@Autowired
	private BrokerService brokerService;	// Added in order to ensure that BrokerService will be instantiated first
	@Autowired
	private ActiveMQConnectionFactory connectionFactory;
	@Autowired
	private BrokerCepService brokerCerService;
	@Autowired
	private PasswordUtil passwordUtil;

	private Connection connection;
	private Session session;

	@Override
	public void afterPropertiesSet() {
		initialize();
	}
	
	protected void initialize() {
		log.debug("BrokerAdvisoryWatcher.init(): Initializing instance...");
		try {
			boolean usesAuthentication = brokerCerService.getBrokerCepProperties().isAuthenticationEnabled();
			String username = brokerCerService.getBrokerUsername();
			String password = brokerCerService.getBrokerPassword();
			log.debug("BrokerAdvisoryWatcher.init(): uses-authentication={}, username={}, password={}",
					usesAuthentication, username, passwordUtil.encodePassword(password));

			this.connection = usesAuthentication
					? connectionFactory.createConnection(username, password)
					: connectionFactory.createConnection();
			this.connection.start();
			this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic topic = session.createTopic("ActiveMQ.Advisory.>");
			MessageConsumer consumer = session.createConsumer(topic);
			consumer.setMessageListener( this );
			log.debug("BrokerAdvisoryWatcher.init(): Initializing instance... done");
		} catch (Exception ex) {
			log.error("BrokerAdvisoryWatcher.init(): EXCEPTION: ", ex);
		}
	}
	
	@Override
	public void onMessage(Message message) {
		try {
			log.trace("BrokerAdvisoryWatcher.onMessage(): {}", message);
			ActiveMQMessage mesg = (ActiveMQMessage) message;
			ActiveMQDestination messageDestination = mesg.getDestination();
			log.trace("BrokerAdvisoryWatcher.onMessage(): advisory-message-source={}", messageDestination);
			
			DataStructure ds = mesg.getDataStructure();
			log.trace("BrokerAdvisoryWatcher.onMessage(): advisory-message-data-structure={}", ds==null ? null : ds.getClass().getSimpleName());
			if (ds!=null) {
				// Advisory event
				processAdvisoryMessage(ds);
			} else {
				// Non-advisory event
				processPlainMessage(mesg);
			}
		} catch (Exception ex) {
			log.error("BrokerAdvisoryWatcher.onMessage(): EXCEPTION: ", ex);
		}
	}

	private void processPlainMessage(ActiveMQMessage mesg) throws JMSException {
		if (mesg instanceof TextMessage) {
			TextMessage txtMesg = (TextMessage) mesg;
			String topicName = mesg.getDestination().getPhysicalName();
			log.info("BrokerAdvisoryWatcher.onMessage(): Text Message received: topic={}, message={}", topicName, txtMesg.getText());
		} else {
			String topicName = mesg.getDestination().getPhysicalName();
			log.info("BrokerAdvisoryWatcher.onMessage(): Non-text Message received: topic={}, type={}", topicName, mesg.getClass().getName());
		}
	}

	private void processAdvisoryMessage(DataStructure ds) throws JMSException {
		if (ds instanceof DestinationInfo) {
			DestinationInfo info = (DestinationInfo) ds;
			ActiveMQDestination destination = info.getDestination();
			boolean isAdd = info.isAddOperation();
			boolean isDel = info.isRemoveOperation();
			log.debug("BrokerAdvisoryWatcher.onMessage(): Received a DestinationInfo message: destination={}, is-queue={}, is-topic={}, is-add={}, is-del={}",
					destination, destination.isQueue(), destination.isTopic(), isAdd, isDel);

			// Subscribe to topic
			if (isAdd) {
				String topicName = destination.getPhysicalName();
				log.info("BrokerAdvisoryWatcher.onMessage(): Subscribing to topic: {}", topicName);

				MessageConsumer consumer = session.createConsumer(destination);
				consumer.setMessageListener(this);
			}
			/*if (isDel) {
				String topicName = destination.getPhysicalName();
				log.info("BrokerAdvisoryWatcher.onMessage(): Leaving topic: {}", topicName);
			}*/

		} else {
			log.trace("BrokerAdvisoryWatcher.onMessage(): Message ignored");
		}
	}
}