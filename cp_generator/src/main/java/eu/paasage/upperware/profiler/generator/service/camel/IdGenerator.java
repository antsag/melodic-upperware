package eu.paasage.upperware.profiler.generator.service.camel;

public interface IdGenerator {

    String generate();
    void reset();
}
