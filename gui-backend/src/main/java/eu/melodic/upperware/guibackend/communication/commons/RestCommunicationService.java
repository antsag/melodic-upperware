package eu.melodic.upperware.guibackend.communication.commons;

import com.google.gson.Gson;
import eu.melodic.upperware.guibackend.communication.jwt.server.response.JwtExceptionResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Slf4j
@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class RestCommunicationService {

    private RestTemplate restTemplate;

    public <T, G> ResponseEntity<T> getResponse(String requestUrl, ParameterizedTypeReference<T> responseType,
                                                HttpEntity<G> requestBody, String serviceName, HttpMethod httpMethod) {
        ResponseEntity<T> response;

        try {
            response = restTemplate.exchange(requestUrl, httpMethod, requestBody, responseType);
        } catch (ResourceAccessException ex) {
            log.error("Error by sending http request to {}: ", serviceName, ex);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Problem in communication with %s. Service not working or connection is too slow. Please try again.", serviceName));
        } catch (HttpServerErrorException ex) {
            log.error("Error by sending http request to {}: ", serviceName, ex);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested resource doesn't exist");
        } catch (HttpClientErrorException ex) {
            log.error("Error by sending http request to {}: ", serviceName, ex);
            String jwtExceptionResponse = getJwtExceptionResponse(ex.getResponseBodyAsString())
                    .map(JwtExceptionResponse::getMessage)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Authorization failed. Invalid credentials. Your account will be locked after 4 fail attempts."));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, jwtExceptionResponse);
        }

        if ((HttpMethod.GET.equals(httpMethod) || HttpMethod.POST.equals(httpMethod))
                && ((!HttpStatus.OK.equals(response.getStatusCode()) && !HttpStatus.CREATED.equals(response.getStatusCode()))
                || response.getBody() == null)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Problem in communication with %s. Service not working. Please try again.", serviceName));
        }

        return response;
    }

    public <T> HttpEntity<T> createHttpEntityWithAuthorizationHeader(T request, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, token);
        return new HttpEntity<>(request, headers);
    }

    public HttpEntity<Void> createEmptyHttpEntityWithAuthorizationHeader(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, token);
        return new HttpEntity<>(headers);
    }

    private Optional<JwtExceptionResponse> getJwtExceptionResponse(String responseBody) {
        Gson gson = new Gson();
        JwtExceptionResponse jwtExceptionResponse = gson.fromJson(responseBody, JwtExceptionResponse.class);
        return Optional.ofNullable(jwtExceptionResponse);
    }

}
