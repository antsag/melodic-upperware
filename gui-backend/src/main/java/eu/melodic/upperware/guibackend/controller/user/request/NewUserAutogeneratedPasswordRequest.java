package eu.melodic.upperware.guibackend.controller.user.request;

import eu.melodic.upperware.guibackend.model.user.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewUserAutogeneratedPasswordRequest {

    private String username;

    private UserRole userRole;
}
