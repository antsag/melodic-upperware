package eu.melodic.upperware.dlms.exception;

public class DLMSException extends RuntimeException {

    public DLMSException(String message) {
        super(message);
    }
}
