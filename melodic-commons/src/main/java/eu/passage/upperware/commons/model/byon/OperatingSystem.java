package eu.passage.upperware.commons.model.byon;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OperatingSystem extends io.github.cloudiator.rest.model.OperatingSystem {
    private long id;
}
