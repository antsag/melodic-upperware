package eu.passage.upperware.commons.model.provider;

public enum CloudType {
    PRIVATE, PUBLIC
}
