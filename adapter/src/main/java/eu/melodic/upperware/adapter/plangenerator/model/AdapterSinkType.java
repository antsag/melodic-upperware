package eu.melodic.upperware.adapter.plangenerator.model;

public enum AdapterSinkType {
    KAIROS_DB, INFLUX, JMS
}
