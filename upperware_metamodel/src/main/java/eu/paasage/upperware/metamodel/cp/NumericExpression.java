/**
 */
package eu.paasage.upperware.metamodel.cp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.paasage.upperware.metamodel.cp.CpPackage#getNumericExpression()
 * @model abstract="true"
 * @generated
 */
public interface NumericExpression extends Expression {
} // NumericExpression
