package eu.paasage.upperware.security.server.data.repository;

public enum UserRole {
    ADMIN, USER, TECHNICAL_USER
}
