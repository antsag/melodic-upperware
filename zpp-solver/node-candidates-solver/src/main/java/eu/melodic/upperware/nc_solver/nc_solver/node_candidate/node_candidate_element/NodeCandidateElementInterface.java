package eu.melodic.upperware.nc_solver.nc_solver.node_candidate.node_candidate_element;

public interface NodeCandidateElementInterface {
    @Override
    int hashCode();
    @Override
    boolean equals(Object obj);
}
